import axios from "axios";

export default {
  namespaced: true,

  state: {
    materials: [],
    outfits: [],
    headshots: []
  },

  mutations: {
    setMaterials(state, materials) {
      state.materials = materials;
    },

    setOutfit(state, outfits) {
      state.outfits = outfits;
    },

    setHeadshots(state, headshots) {
      state.headshots = headshots;
    }
  },

  actions: {
    updateMaterials({ commit }, data) {
      return axios.post("materials", data).then(response => {
        commit("setMaterials", response.data);
      });
    },

    getMaterials({ commit }) {
      return axios.get("materials").then(response => {
        commit("setMaterials", response.data.data);
      });
    },
    updateOutfit({ commit }, data) {
      return axios.post("outfits", data).then(response => {
        commit("setOutfit", response.data);
      });
    },

    getOutfit({ commit }) {
      return axios.get("outfits").then(response => {
        commit("setOutfit", response.data.data);
      });
    },
    getHeadshots({ commit }) {
      return axios.get("headshots").then(response => {
        commit("setHeadshots", response.data);
      });
    },
    // eslint-disable-next-line
    deleteHeadshots({}, id) {
      return axios.delete(`headshots/${id}`);
    },
    // eslint-disable-next-line
    uploadHeadshot({}, data) {
      return axios.post("headshots", data).then(response => {
        console.log(response);
      });
    }
  },
  getters: {
    userOutfit: state => {
      return state.outfits;
    },
    userMaterials: state => {
      return state.materials;
    },
    userHeadshots: state => {
      return state.headshots;
    }
  }
};

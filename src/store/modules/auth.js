import axios from "axios";
import Vue from "vue";
import router from "../../router";

export default {
  namespaced: true,

  state: {
    user: {}
  },

  mutations: {
    setUser(state, user) {
      state.user = user;
    },

    clearUser(state) {
      state.user = {};
    }
  },

  actions: {
    // eslint-disable-next-line
    setToken({}, token) {
      Vue.localStorage.set("token", token);
      axios.defaults.headers.common["Authorization"] = "Bearer " + token;
    },

    clearToken() {
      Vue.localStorage.remove("token");
      axios.defaults.headers.common["Authorization"] = null;
    },
    // eslint-disable-next-line
    register({}, data) {
      return axios.post("register", data);
    },
    // eslint-disable-next-line
    forgotPass({}, data) {
      return axios.post("password/restore", data);
    },
    // eslint-disable-next-line
    resetPass({}, data) {
      return axios.post("password/reset", data);
    },

    registerConfirm({ dispatch, commit }, data) {
      return axios.post("register/confirm", data).then(response => {
        dispatch("setToken", response.data.token.accessToken);
        commit("setUser", response.data.user);
      });
    },
    login({ dispatch }, data) {
      return axios.post("login", data).then(response => {
        dispatch("setToken", response.data.token.accessToken);
      });
    },

    getProfile({ commit }) {
      return axios.get("profile").then(response => {
        commit("setUser", response.data);
      });
    },
    updateProfile({ commit }, data) {
      return axios.put("profile", data).then(response => {
        commit("setUser", response.data.user);
        return response.data;
      });
    },
    logout({ commit, dispatch }) {
      commit("clearUser");
      dispatch("clearToken");
      router.push("/login");
    }
  },
  getters: {
    authenticated: state => {
      return Object.keys(state.user).length > 0;
    },
    userProfile: state => {
      return state.user;
    }
  }
};

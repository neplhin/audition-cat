import axios from "axios";

export default {
  namespaced: true,

  state: {
    auditions: [],
    initialAudition: {}
  },

  mutations: {
    setAuditions(state, auditions) {
      state.auditions = auditions;
    },
    setInitialAudition(state, initialAudition) {
      state.initialAudition = initialAudition;
    }
  },

  actions: {
    getAuditions({ commit }) {
      return axios.get("auditions").then(response => {
        commit("setAuditions", response.data);
      });
    },
    // eslint-disable-next-line
    getAudition({}, id) {
      return axios.get(`auditions/${id}`);
    },
    // eslint-disable-next-line
    postAuditions({}, data) {
      return axios.post("auditions", data);
    },
    // eslint-disable-next-line
    deleteAudition({}, id) {
      return axios.delete(`auditions/${id}`);
    },
    getInitialAudition({ commit }) {
      return axios.get("initial/audition").then(response => {
        commit("setInitialAudition", response.data.data);
      });
    }
  },
  getters: {
    userAuditions: state => {
      return state.auditions;
    },
    userInitialAudition: state => {
      return state.initialAudition;
    }
  }
};

import Vue from "vue";
import Vuex from "vuex";
import auth from "./modules/auth";
import userInfo from "./modules/userInfo";
import auditions from "./modules/auditions";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    userInfo,
    auditions
  }
});

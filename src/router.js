import Vue from "vue";
import Router from "vue-router";
import Home from "./pages/Home.vue";
import store from "./store";

Vue.use(Router);

const middleware = {
  auth: (to, from, next) => {
    let authenticated = store.getters["auth/authenticated"];
    let token = Vue.localStorage.get("token");

    if (authenticated) {
      next();
    } else {
      if (token) {
        store
          .dispatch("auth/setToken", token)
          .then(() => store.dispatch("auth/getProfile").then(next));
      } else {
        next("/login");
      }
    }
  },

  guest: (to, from, next) => {
    if (Vue.localStorage.get("token")) {
      next("/profile");
    } else {
      next();
    }
  }
};

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/register",
      name: "register",
      component: () => import("./pages/auth/Register.vue"),
      beforeEnter: middleware.guest
    },
    {
      path: "/register/confirm",
      name: "register confirmation",
      component: () => import("./pages/auth/RegisterConfirm.vue"),
      beforeEnter: middleware.guest
    },
    {
      path: "/login",
      name: "login",
      component: () => import("./pages/auth/Login.vue"),
      beforeEnter: middleware.guest
    },
    {
      path: "/dashboard",
      name: "dashboard",
      component: () => import("./pages/Dashboard.vue"),
      beforeEnter: middleware.auth
    },
    {
      path: "/profile",
      name: "profile",
      component: () => import("./pages/Profile.vue"),
      beforeEnter: middleware.auth
    },
    {
      path: "*",
      name: "error",
      component: () => import("./pages/Error.vue")
    },
    {
      path: "/forgot-password",
      name: "ForgotPass",
      component: () => import("./pages/auth/ForgotPassword.vue"),
      beforeEnter: middleware.guest
    },
    {
      path: "/forgot-password/check-email",
      name: "Checkemail",
      component: () => import("./pages/auth/CheckEmail.vue"),
      beforeEnter: middleware.guest
    },
    {
      path: "/reset-password",
      name: "ResetPass",
      component: () => import("./pages/auth/ResetPassword.vue"),
      beforeEnter: middleware.guest
    },
    {
      path: "/auditions",
      name: "Auditions",
      component: () => import("./pages/Auditions.vue"),
      beforeEnter: middleware.auth
    }
  ]
});

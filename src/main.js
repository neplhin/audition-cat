import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import axios from "axios";
import VueLocalStorage from "vue-localstorage";
import VeeValidate from "vee-validate";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";

Vue.use(Vuetify);

Vue.config.productionTip = false;

//const api_version = "v1";

// Setting Axios defaults
axios.defaults.baseURL = process.env.VUE_APP_API_URL + "/" /*+ api_version*/;

// Add a request interceptor
axios.interceptors.request.use(
  function(config) {
    // Do something before request is sent
    return config;
  },
  function(error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
axios.interceptors.response.use(
  function(response) {
    // Do something with response data
    return response;
  },
  function(error) {
    // Do something with response error

    if (error.response.data.message === "Unauthorized") {
      store.dispatch("auth/clearToken");
      router.push("/login");
    }

    return Promise.reject(error);
  }
);

// Plugins
Vue.use(VueLocalStorage);
Vue.use(VeeValidate, {
  events: "blur"
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
